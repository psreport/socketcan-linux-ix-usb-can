# socketcan-linux-ix-usb-can

Fixes/updates for IXXAT USB-to-CAN Driver

This is a copy of the official Ixxat ([1]) USB-to-CAN Driver for linux
obtained from the Ixxat downlaod page ([2]) (ix_usb_can_2.0.366-REL.tgz
downloaded from [3]) with fixes for compile with linux version >= 5.12.0
and CAN FD usage.

## Branch [branch ix_usb_can_2.0.366-REL](/psreport/socketcan-linux-ix-usb-can/src/branch/ix_usb_can_2.0.366-REL):

- c5464ac can: ixxat_usb: adjust for can_..._skb frame_len changes (2023-04-26)
- 51b3021 can: ixxat_usb: do not free skb before last usage (2023-05-14)

## Branch [branch linux-can_patches](/psreport/socketcan-linux-ix-usb-can/src/branch/linux-can_patches):

- 9bf8e10 [PATCH] can: usb: IXXAT USB-to-CAN adapters drivers
- 0a63b42 [PATCH v2] can: usb: IXXAT USB-to-CAN adapters drivers
- 7cb3985 [PATCH v3] can: usb: IXXAT USB-to-CAN adapters drivers
- 1c23a2f [PATCH v4] can: usb: IXXAT USB-to-CAN adapters drivers
- 421fe09 [PATCH v5] can: usb: IXXAT USB-to-CAN adapters drivers
- c506e71 [PATCH v6] can: usb: IXXAT USB-to-CAN adapters drivers
- 1723ca0 [PATCH v7] can: usb: IXXAT USB-to-CAN adapters drivers
- 01cbc33 [PATCH v8] can: usb: IXXAT USB-to-CAN adapters drivers
- 83819b6 [PATCH v9] can: usb: IXXAT USB-to-CAN adapters drivers
- fa8cbf7 [PATCH v10 1/3] can: usb: IXXAT USB-to-CAN adapters drivers
- 0fe167e [PATCH v10 2/3] can: usb: ixxat_usb: add USB2CAN_PLUGIN_PRODUCT_ID (IXXAT USB Plugin)
- 7baecc6 [PATCH v10 3/3] can: usb: ixxat_usb: promote legacy adapters with up-todate firmware to cl2

## Branch [branch linux-can_patches-v7-vs-ix_usb_can_2.0.366-REL](/psreport/socketcan-linux-ix-usb-can/src/branch/):

Changes between '[PATCH v7] can: usb: IXXAT USB-to-CAN adapters drivers' and
the offical 'ix_usb_can_2.0.366-REL' release:

- 7c327a5 ix_usb_can_2.0.366-REL

## Notes

- Found the following references to bring the Ixxat USB-to-CAN Driver for
  linux upstream (dating back to 2018, all by Florian Ferg) but no suggested
  patch seems to be successful yet:

  - 2018-06-08:
    [PATCH] can: usb: IXXAT USB-to-CAN adapters drivers

    https://marc.info/?l=linux-can&m=152845162427920

  - 2018-06-25:
    [PATCH v2] can: usb: IXXAT USB-to-CAN adapters drivers

    https://marc.info/?l=linux-can&m=152996258423265

  - 2018-06-28:
    [PATCH v3] can: usb: IXXAT USB-to-CAN adapters drivers

    https://marc.info/?l=linux-can&m=153019414431266

  - 2018-07-09:
    [PATCH v4] can: usb: IXXAT USB-to-CAN adapters drivers

    https://marc.info/?l=linux-can&m=153113773029121

  - 2018-08-10:
    [PATCH v5] can: usb: IXXAT USB-to-CAN adapters drivers

    https://marc.info/?l=linux-can&m=153390928701300

  - 2018-09-03:
    [PATCH v6] can: usb: IXXAT USB-to-CAN adapters drivers

    https://marc.info/?l=linux-can&m=153596357327489

  - 2018-09-27:
    [PATCH v7] can: usb: IXXAT USB-to-CAN adapters drivers

    https://marc.info/?l=linux-can&m=153805714622658

  After asking on the linux-can mailing list for possible updates
  (see [5]) Marc Kleine-Budde posted an updated version:

  - 2023-05-15:
    [PATCH v8] can: usb: IXXAT USB-to-CAN adapters drivers

    https://lore.kernel.org/linux-can/20230515212930.1019702-1-mkl@pengutronix.de/

  Updated version from Peter Seiderer after review by Vincent Mailhol:

  - 2023-05-22:
    [PATCH v9] can: usb: IXXAT USB-to-CAN adapters drivers
    https://lore.kernel.org/linux-can/20230522200144.15949-1-ps.report@gmx.net/

  - 2023-07-07:
    [PATCH v10 1/3] can: usb: IXXAT USB-to-CAN adapters drivers
    https://lore.kernel.org/linux-can/20230707171412.31195-2-ps.report@gmx.net/
    [PATCH v10 2/3] can: usb: ixxat_usb: add USB2CAN_PLUGIN_PRODUCT_ID (IXXAT USB Plugin)
    https://lore.kernel.org/linux-can/20230707171412.31195-3-ps.report@gmx.net/
    [PATCH v10 3/3] can: usb: ixxat_usb: promote legacy adapters with up-todate firmware to cl2
    https://lore.kernel.org/linux-can/20230707171412.31195-4-ps.report@gmx.net/

- There is another old/legacy git repo for an old version with compile fixes
  (see [4]).

## References

[1] https://www.ixxat.com/

[2] https://www.ixxat.com/de/technical-support/resources/downloads-and-documentation?ordercode=1.01.0351.12001

[3] https://www.ixxat.com/docs/librariesprovider8/ixxat-english-new/pc-can-interfaces/linux-drivers/socketcan-linux.tgz?sfvrsn=3eb48d7_22&download=true

[4] https://github.com/jackthompsonnz/socketcan-linux-5.0

[5] https://lore.kernel.org/linux-can/20230514151221.049873cb@gmx.net/
